<?php

/**
 * @file
 * Install, update and uninstall hooks.
 */

use Drupal\panopoly\Installer\Form\PanopolyDemoInstallerForm;
use Drupal\shortcut\Entity\Shortcut;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;

/**
 * Implements hook_install_tasks().
 */
function panopoly_install_tasks(&$install_tasks) {
  $tasks = [];

  // Add a task to install demo content.
  $tasks[PanopolyDemoInstallerForm::class] = [
    'display_name' => t('Install demo content'),
    'type' => 'form',
  ];

  // Add the Panopoly theme selection to the installation process.
  if (_panopoly_load_module_include('inc', 'panopoly_theme', 'panopoly_theme.profile')) {
    $tasks += panopoly_theme_profile_theme_selection_install_task($install_state);
  }

  return $tasks;
}

/**
 * Loads an include file (without requiring that the module is enabled).
 *
 * @param string $type
 *   File extension.
 * @param string $module
 *   Module name.
 * @param string $name
 *   File name (without the extension).
 *
 * @return bool
 *   TRUE if load was successful; otherwise FALSE.
 */
function _panopoly_load_module_include($type, $module, $name) {
  /** @var \Drupal\Core\Extension\ModuleExtensionList $module_list */
  $module_list = \Drupal::service('extension.list.module');
  $file = DRUPAL_ROOT . '/' . $module_list->getPath($module) . "/$name.$type";
  if (is_file($file)) {
    require_once $file;
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_install().
 *
 * Perform actions to set up the site for this profile.
 *
 * @see system_install()
 */
function panopoly_install() {
  // Assign user 1 the "administrator" role.
  $user = User::load(1);
  $user->roles[] = 'administrator';
  $user->save();

  // Grant the 'use panopoly_search' permission to anonymous users by default.
  $role = Role::load(Role::ANONYMOUS_ID);
  $role->grantPermission('use panopoly_search');
  $role->save();

  // We install some menu links, so we have to rebuild the router, to ensure the
  // menu links are valid.
  \Drupal::service('router.builder')->rebuildIfNeeded();

  // Populate the default shortcut set.
  $shortcut = Shortcut::create([
    'shortcut_set' => 'default',
    'title' => t('Add content'),
    'weight' => -20,
    'link' => ['uri' => 'internal:/node/add'],
  ]);
  $shortcut->save();

  $shortcut = Shortcut::create([
    'shortcut_set' => 'default',
    'title' => t('All content'),
    'weight' => -19,
    'link' => ['uri' => 'internal:/admin/content'],
  ]);
  $shortcut->save();
}

/**
 * Implements hook_update_last_removed().
 */
function panopoly_update_last_removed() {
  return 8001;
}
