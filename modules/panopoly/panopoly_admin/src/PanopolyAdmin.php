<?php

namespace Drupal\panopoly_admin;

use Drupal\panopoly_admin\Event\AdvancedBlockEvent;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * The Panopoly Admin service.
 */
class PanopolyAdmin {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs the Panopoly Admin service.
   *
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(EventDispatcherInterface $event_dispatcher) {
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Gets the advanced blocks.
   *
   * @param array $block_definitions
   *   The block definitions.
   *
   * @return array
   *   An associative array of the advanced blocks, keyed by the plugin id with
   *   a truthy value.
   */
  public function getAdvancedBlocks(array $block_definitions) {
    $event = new AdvancedBlockEvent($block_definitions);
    $this->eventDispatcher->dispatch($event);
    return $event->getAdvanced();
  }

}
