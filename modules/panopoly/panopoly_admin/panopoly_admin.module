<?php

/**
 * @file
 * Hooks for Panopoly Admin.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_plugin_filter_TYPE__CONSUMER_alter().
 */
function panopoly_admin_plugin_filter_layout__layout_builder_alter(array &$definitions, array $extra) {
  // Don't do any filtering if this is being called by our admin form.
  if (isset($extra['panopoly_admin_layouts_form']) && $extra['panopoly_admin_layouts_form']) {
    return;
  }

  $config = \Drupal::config('panopoly_admin.settings');
  $layouts = $config->get('layouts');

  // Remove disabled layouts.
  foreach ($definitions as $id => $definition) {
    if (isset($layouts[$id]) && !$layouts[$id]) {
      unset($definitions[$id]);
    }
  }
}

/**
 * Implements hook_plugin_filter_TYPE__CONSUMER_alter().
 */
function panopoly_admin_plugin_filter_block__layout_builder_alter(array &$definitions, array $extra) {
  // Don't do any filtering if this is being called by our admin form.
  if (isset($extra['panopoly_admin_blocks_form']) && $extra['panopoly_admin_blocks_form']) {
    return;
  }

  $show_hidden_blocks = \Drupal::request()->query->get('panopoly_admin_show_hidden_blocks', FALSE);
  if (\Drupal::currentUser()->hasPermission('use blocks hidden by panopoly_admin') && $show_hidden_blocks) {
    return;
  }

  $config = \Drupal::config('panopoly_admin.settings');
  $blocks = $config->get('blocks') ?? [];
  $block_categories = $config->get('block_categories') ?? [];
  $show_advanced_blocks = $config->get('show_advanced_blocks') ?? FALSE;

  // Don't bother grabbing the advanced blocks if we're not going to use them.
  $advanced_blocks = [];
  if (!$show_advanced_blocks) {
    $advanced_blocks = \Drupal::service('panopoly_admin')->getAdvancedBlocks($definitions);
  }

  // Remove disabled blocks.
  foreach ($definitions as $id => $definition) {
    if (isset($blocks[$id]) && !$blocks[$id]) {
      unset($definitions[$id]);
      continue;
    }

    if (!$show_advanced_blocks && isset($advanced_blocks[$id])) {
      unset($definitions[$id]);
      continue;
    }

    $category = (string) $definitions[$id]['category'] ?? '';
    if (isset($block_categories[$category]) && !$block_categories[$category]) {
      unset($definitions[$id]);
      continue;
    }
  }
}

/**
 * Implements hook_link_alter().
 */
function panopoly_admin_link_alter(&$variables) {
  /** @var Drupal\Core\Url $url */
  $url = $variables['url'];

  if (!$url->isRouted()) {
    return;
  }

  if ($url->getRouteName() !== 'layout_builder.choose_block') {
    return;
  }

  if (!\Drupal::request()->query->get('panopoly_admin_show_hidden_blocks', FALSE)) {
    return;
  }

  $route_params = $url->getRouteParameters();
  if (isset($route_params['panopoly_admin_show_hidden_blocks'])) {
    // Don't alter any explicit values.
    return;
  }

  $route_params['panopoly_admin_show_hidden_blocks'] = TRUE;
  $url->setRouteParameters($route_params);
}

/**
 * Implements hook_module_implements_alter().
 */
function panopoly_admin_module_implements_alter(&$implementations, $hook) {
  if ($hook === 'plugin_filter_block__layout_builder_alter') {
    // We need our filtering to run after any other filtering, so that we can
    // get the most up-to-date category names, in case a different hook alters
    // them.
    $group = $implementations['panopoly_admin'];
    unset($implementations['panopoly_admin']);
    $implementations['panopoly_admin'] = $group;
  }
}

/**
 * Implements hook_help().
 */
function panopoly_admin_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'panopoly_admin.panopoly_admin_blocks_form':
      return '<p>' . t("Here you can enable or disable which blocks are available in Layout Builder for users who don't have the <em>'Use hidden blocks'</em> permission.") . '</p>';
  }
}
