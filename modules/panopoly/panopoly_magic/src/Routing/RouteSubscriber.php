<?php

namespace Drupal\panopoly_magic\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Implements a route subscriber for Panopoly Magic.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Replace the layout builder choose block and section routes with our own.
    if ($route = $collection->get('layout_builder.choose_block')) {
      $route->setDefaults([
        '_controller' => '\Drupal\panopoly_magic\Controller\LayoutBuilderChooseBlockController::chooseBlock',
        '_title' => $route->getDefault('_title'),
      ]);
    }
    if ($route = $collection->get('layout_builder.choose_section')) {
      $route->setDefaults([
        '_controller' => '\Drupal\panopoly_magic\Controller\LayoutBuilderChooseSectionController::build',
        '_title' => $route->getDefault('_title'),
      ]);
    }

    // Replace the layout builder add and update block form with our own.
    if ($route = $collection->get('layout_builder.add_block')) {
      $route->setDefault('_controller', '\Drupal\panopoly_magic\Controller\LivePreviewFormController::getForm');
      $route->setDefault('_form', '\Drupal\panopoly_magic\Form\LayoutBuilderAddBlockForm');
    }
    if ($route = $collection->get('layout_builder.update_block')) {
      $route->setDefault('_controller', '\Drupal\panopoly_magic\Controller\LivePreviewFormController::getForm');
      $route->setDefault('_form', '\Drupal\panopoly_magic\Form\LayoutBuilderUpdateBlockForm');
    }
  }

}
