<?php

/**
 * @file
 * Install hooks for Panopoly Media.
 */

use Drupal\system\Entity\Action;

/**
 * Implements hook_install().
 */
function panopoly_media_install() {
  _panopoly_media_set_delete_config();
}

/**
 * Adds the configuration entity for the file delete action.
 */
function _panopoly_media_set_delete_config() {
  if ($action = Action::load('panopoly_media_file_delete_action')) {
    return;
  }

  $action = Action::create([
    'langcode' => 'en',
    'status' => TRUE,
    'dependencies' => [
      'module' => [
        'file',
        'panopoly_media',
      ],
    ],
    'id' => 'panopoly_media_file_delete_action',
    'label' => 'Delete file',
    'type' => 'file',
    'plugin' => 'panopoly_media_file_delete_action',
    "configuration" => [],
  ]);
  $action->save();
}

/**
 * Implements hook_update_last_removed().
 */
function panopoly_media_update_last_removed() {
  return 8210;
}

/**
 * Change WYSIWYG embed button icon to SVG.
 */
function panopoly_media_update_93001() {
  /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
  $config_factory = \Drupal::service('config.factory');

  $config = $config_factory->getEditable('embed.button.panopoly_media_wysiwyg_media_embed');
  $config->set('icon.uri', 'public://panopoly_media_icon.svg');
  $config->set('icon.data', 'PHN2ZyB2aWV3Qm94PSIwIDAgMjAgMjAiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTTYuOTEgMTAuNTRjLjI2LS4yMy42NC0uMjEuODguMDNsMy4zNiAzLjE0IDIuMjMtMi4wNmEuNjQuNjQgMCAwIDEgLjg3IDBsMi41MiAyLjk3VjQuNUgzLjJ2MTAuMTJsMy43MS00LjA4em0xMC4yNy03LjUxYy42IDAgMS4wOS40NyAxLjA5IDEuMDV2MTEuODRjMCAuNTktLjQ5IDEuMDYtMS4wOSAxLjA2SDIuNzljLS42IDAtMS4wOS0uNDctMS4wOS0xLjA2VjQuMDhjMC0uNTguNDktMS4wNSAxLjEtMS4wNWgxNC4zOHptLTUuMjIgNS41NmExLjk2IDEuOTYgMCAxIDEgMy40LTEuOTYgMS45NiAxLjk2IDAgMCAxLTMuNCAxLjk2eiI+PC9wYXRoPjwvc3ZnPgo=');
  $config->save();
}
