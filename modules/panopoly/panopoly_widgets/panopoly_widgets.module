<?php

/**
 * @file
 * Hooks for Panopoly Widgets module.
 */

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * The default list of allowed file extensions in file widgets.
 */
define('PANOPOLY_WIDGETS_FILE_EXTENSIONS_DEFAULT', 'txt doc docx xls xlsx pdf ppt pptx pps ppsx odt ods odp');

define('PANOPOLY_WIDGETS_IMAGE_BUNDLE', 'panopoly_widgets_image');

/**
 * Implements hook_entity_view_alter().
 */
function panopoly_widgets_entity_view_alter(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display) {
  if ($entity->getEntityTypeId() === 'block_content' && $entity->bundle() === PANOPOLY_WIDGETS_IMAGE_BUNDLE) {
    $link_uri = $entity->field_panopoly_widgets_link->uri;
    if (!empty($link_uri)) {
      $url = Url::fromUri($link_uri);
      $build["field_panopoly_widgets_image"][0]["#url"] = $url;
    }
  }
}

/**
 * Implements hook_block_alter().
 */
function panopoly_widgets_block_alter(&$definitions) {
  $custom_blocks = [
    'inline_block:panopoly_widgets_file',
    'inline_block:panopoly_widgets_image',
    'inline_block:panopoly_widgets_links',
    'inline_block:panopoly_widgets_text',
  ];
  foreach ($custom_blocks as $custom_block) {
    if (isset($definitions[$custom_block])) {
      $definitions[$custom_block]['category'] = t('Custom');
    }
  }
}

/**
 * Implements hook_plugin_filter_TYPE__CONSUMER_alter().
 */
function panopoly_widgets_plugin_filter_block__layout_builder_alter(array &$definitions, array $extra) {
  // Use the 'menu_block:main' block as the Submenu if it's available, but if
  // not fallback on 'system_menu_block:main'.
  foreach (['menu_block:main', 'system_menu_block:main'] as $plugin_id) {
    if (isset($definitions[$plugin_id])) {
      $definitions[$plugin_id]['category'] = t('Custom');
      $definitions[$plugin_id]['admin_label'] = t('Submenu');
      break;
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function panopoly_widgets_form_layout_builder_add_block_alter(array &$form, FormStateInterface $form_state, $form_id) {
  $build_info = $form_state->getBuildInfo();
  $plugin_id = $build_info['args'][3];

  _panopoly_widgets_layout_builder_block_form_alter($form, $form_state, $plugin_id);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function panopoly_widgets_form_layout_builder_update_block_alter(array &$form, FormStateInterface $form_state, $form_id) {
  $build_info = $form_state->getBuildInfo();
  [$section_storage, $delta, $region, $uuid] = $build_info['args'];
  /** @var \Drupal\layout_builder\SectionComponent $component */
  $component = $section_storage->getSection($delta)->getComponent($uuid);
  $plugin_id = $component->getPluginId();

  _panopoly_widgets_layout_builder_block_form_alter($form, $form_state, $plugin_id);
}

/**
 * Alters the add or update block forms in layout builder.
 *
 * @param array $form
 *   The form.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 * @param string $plugin_id
 *   The block plugin id.
 */
function _panopoly_widgets_layout_builder_block_form_alter(array &$form, FormStateInterface $form_state, $plugin_id) {
  if ($plugin_id === 'system_menu_block:main') {
    // Make the 'Menu levels' not collapsible.
    $form['settings']['menu_levels']['#type'] = 'container';
  }
  elseif ($plugin_id === 'menu_block:main') {
    // Make the 'Menu levels' and 'Advanced options' not collapsible.
    $form['settings']['menu_levels']['#type'] = 'container';
    $form['settings']['advanced']['#type'] = 'container';

    // Force the theme suggestion to be 'submenu'.
    $form['settings']['style']['#access'] = FALSE;
    $form['settings']['style']['suggestion']['#default_value'] = 'submenu';
  }
}
