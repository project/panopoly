<?php

/**
 * @file
 * Installation hooks for the panopoly_core module.
 */

declare(strict_types=1);

use Drupal\user\Entity\Role;

/**
 * Implements hook_install().
 */
function panopoly_core_install($is_syncing) {
  // If we're not being installed as part of a configuration sync, ensure that
  // the Pathauto settings are as we would expect them to be.
  if (!$is_syncing) {
    $config_factory = \Drupal::configFactory();
    $pathauto_settings = $config_factory->getEditable('pathauto.settings');
    $pathauto_settings->set('update_action', 1);
    $pathauto_settings->save();

    // Create here rather than via config, so that its permissions can't get
    // reverted, and Features doesn't have issues with this being in the profile
    // config.
    $editor_role = Role::load('editor');
    if (!$editor_role) {
      $editor_role = Role::create([
        'id' => 'editor',
        'label' => 'Editor',
        'weight' => 2,
        'is_admin' => FALSE,
        'status' => TRUE,
        'langcode' => 'en',
      ]);

      $editor_permissions = [
        'access administration pages',
        'access content',
        'access content overview',
        'access toolbar',
        'view own unpublished content',
      ];
      foreach ($editor_permissions as $permission) {
        $editor_role->grantPermission($permission);
      }

      $editor_role->save();
    }
  }
}

/**
 * Implements hook_update_last_removed().
 */
function panopoly_core_update_last_removed() {
  return 8201;
}
