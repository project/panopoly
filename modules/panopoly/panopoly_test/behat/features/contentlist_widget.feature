Feature: Add content list widget
  In order to create a list of certain content
  As a site administrator
  I need to be able to add a list with the content I choose

  @api @javascript @panopoly_widgets
  Scenario: Add a content list
    Given I am logged in as a user with the "administrator" role
      And "panopoly_test_content_page" content:
      | title       | body      | created            | status |
      | Test Page 3 | Test body | 01/01/2001 11:00am |      1 |
      | Test Page 1 | Test body | 01/02/2001 11:00am |      1 |
      | Test Page 2 | Test body | 01/03/2001 11:00am |      1 |
      And I am viewing a landing page
    When I click "Layout"
      And I click "Add block in Section 1, Content region"
      And I click "Add List of content" in the "Offcanvas dialog"
    When I fill in the following:
       | settings[views_label_checkbox] | 1 |
       | settings[views_label]    | Content Page List Asc 1 |
       | settings[override][items_per_page]  | 1                       |
#      | Display Type    | Fields                  |
#    When I select "Test Page" from "exposed[type]"
#      And I select "Asc" from "exposed[sort_order]"
#      And I select "Title" from "exposed[sort_by]"
      And I wait 5 seconds
      And I press "Save" in the "Offcanvas dialog" region
        And I press "Save layout"
      And I wait 5 seconds
    Then I should see "Content Page List Asc 1"
      And I should see "Test Page 1"
      And I should see "January 2, 2001"
      And I should see "Posted by Anonymous"
    # Check that 'Sort by' stays set, per #2153291
    When I customize this page with the Panels IPE
     And I click "Settings" in the "Boxton Content" region
    Then I should see "Configure Add content list"
      And the "exposed[sort_by]" field should contain "Title"
