<?php

/**
 * @file
 * Install hooks for Panopoly WYSIWYG.
 */

use Drupal\Core\Config\FileStorage;
use Drupal\user\Entity\Role;

/**
 * Implements hook_install().
 */
function panopoly_wysiwyg_install() {

  // Create here rather than via config, so that its permissions can't get
  // reverted, and Features doesn't have issues with overridden config.
  $editor_role = Role::load('editor');
  if (!$editor_role) {
    $editor_role = Role::create([
      'id' => 'editor',
      'label' => 'Editor',
      'weight' => 2,
      'is_admin' => FALSE,
      'status' => TRUE,
      'langcode' => 'en',
    ]);
    $editor_role->save();
  }

}

/**
 * Implements hook_modules_installed().
 */
function panopoly_wysiwyg_modules_installed(array $modules) {

  user_role_grant_permissions('anonymous', [
    'use text format restricted_html',
  ]);
  user_role_grant_permissions('authenticated', [
    'use text format restricted_html',
    'use text format panopoly_wysiwyg_basic',
  ]);
  user_role_grant_permissions('editor', [
    'use text format restricted_html',
    'use text format panopoly_wysiwyg_basic',
  ]);

}

/**
 * Implements hook_update_last_removed().
 */
function panopoly_wysiwyg_update_last_removed() {
  return 8209;
}

/**
 * Update WYSIWYG configuration to CKEditor 5.
 */
function panopoly_wysiwyg_update_93001() {
  \Drupal::service('module_installer')->install([
    'ckeditor5',
    'anchor_link',
  ]);

  /** @var \Drupal\Core\Extension\ExtensionList $module_list */
  $module_list = \Drupal::service('extension.list.module');
  $config_path = $module_list->getPath('panopoly_wysiwyg') . '/config/update_93001';

  $source = new FileStorage($config_path);
  /** @var \Drupal\Core\Config\StorageInterface $config_storage */
  $config_storage = \Drupal::service('config.storage');

  $config_names = [
    'editor.editor.panopoly_wysiwyg_basic',
    'filter.format.panopoly_wysiwyg_basic',
  ];
  foreach ($config_names as $config_name) {
    $config_storage->write($config_name, $source->read($config_name));
  }
}
